using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class TriggerDialogue : MonoBehaviour
{
	Flowchart flow;
	[SerializeField] GameObject exclamationMark;

	private void OnTriggerEnter(Collider other) {
		if (other.CompareTag("Character")) {
			exclamationMark.SetActive(true);
		}
	}

	private void OnTriggerExit(Collider other) {
		if (other.CompareTag("Character")) {
			exclamationMark.SetActive(false);
		}
	}

	public void EnableFlowchart() {
		flow.gameObject.SetActive(true);
	}

	public void DisableFlowchart() {
		flow.gameObject.SetActive(false);
	}
}
