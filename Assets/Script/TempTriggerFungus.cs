using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using UnityEngine.UI;
using DG.Tweening;

public class TempTriggerFungus : MonoBehaviour
{
	private GameObject rt;
	private Camera cam;

	[SerializeField] private bool hasTalked;
	[SerializeField] private Flowchart flow;
	private static Button talkButton;
	[SerializeField] private TempTriggerFungus theGuy;

    public void Start()
    {
		cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

		if (!talkButton)
		{
			talkButton = GameObject.Find("Talk").GetComponent<Button>();
			talkButton.gameObject.SetActive(false);
		}

		rt = transform.GetChild(0).gameObject;
		rt.transform.rotation = Quaternion.LookRotation(cam.transform.forward);
		rt.transform.GetChild(0).GetComponent<Image>().color = Color.clear;
	}

	private void OnTriggerEnter(Collider other) {
		if (other.CompareTag("Character"))
		{
			rt.transform.GetChild(0).GetComponent<Image>().DOColor(Color.white, 0.25f);
			talkButton.gameObject.SetActive(true);

			talkButton.onClick.AddListener(() => {
				other.GetComponent<PlayerController>().SetTalking(true);
				
				Vector3 target = new Vector3(transform.GetChild(0).transform.position.x, other.transform.position.y, transform.GetChild(0).transform.position.z);
				other.transform.DOLookAt(target, 0.5f);

				if (theGuy) theGuy.hasTalked = true;

				if (hasTalked)
					flow.ExecuteBlock("Initiate");
				else
					flow.ExecuteBlock("Not Talked Yet");
			});
		}
	}

    private void OnTriggerExit(Collider other)
    {
		if (other.CompareTag("Character"))
		{
			rt.transform.GetChild(0).GetComponent<Image>().DOColor(Color.clear, 0.25f);

			talkButton.gameObject.SetActive(false);
			talkButton.onClick.RemoveAllListeners();
		}
	}
}
