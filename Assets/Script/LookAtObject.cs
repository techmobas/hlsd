using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtObject : MonoBehaviour
{
    [SerializeField] GameObject obj;
    void Update()
    {
        transform.rotation = Quaternion.LookRotation(transform.position - obj.transform.position);
    }
}
