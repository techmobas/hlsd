using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TouchManager : MonoBehaviour
{
    private PlayerInput playerInput;

	InputAction touchPositionAction;
	InputAction touchPressAction;

	private void Awake() {
		playerInput = GetComponent<PlayerInput>();
		touchPositionAction = playerInput.actions["TouchPosition"];
		touchPressAction = playerInput.actions["TouchPress"];
	}
	private void OnEnable() {
		touchPressAction.performed += TouchPress;
	}

	private void OnDisable() {
		touchPressAction.performed -= TouchPress;
	}

	void TouchPress(InputAction.CallbackContext context) {
		float value = context.ReadValue<float>();
		Debug.Log(value);
	}
}
 