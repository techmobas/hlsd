using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerController : MonoBehaviour
{
    public static bool isTalking;

    private Animator anim;
    
    private PlayerControl playerControl;
    private CharacterController controller;

    private Vector3 playerVelocity;
    private bool groundedPlayer;

    [SerializeField] private float playerSpeed = 2.0f;
    //[SerializeField] private float jumpHeight = 1.0f;
    //private float gravityValue = -9.81f;


    private void Awake() {
        anim = transform.GetChild(0).GetComponent<Animator>();

        playerControl = new PlayerControl();
        controller = GetComponent<CharacterController>();
	}

	private void OnEnable() {
		playerControl.Enable();
	}

    private void OnDisable() {
        playerControl.Disable();
    }

    private void Start() {
        
    }

    public void SetTalking(bool set) { isTalking = set; }

    void Update() {
        groundedPlayer = controller.isGrounded;
        if (groundedPlayer && playerVelocity.y < 0) {
            playerVelocity.y = 0f;
        }

        Vector2 moveInput = playerControl.MainAction.Move.ReadValue<Vector2>();
        Vector3 move = new Vector3(moveInput.x, 0f, moveInput.y);
        move = Quaternion.AngleAxis(32.694f, Vector3.up) * move;
        anim.SetFloat("walkSpeed", move.magnitude);

        if (!isTalking)
        {
            controller.Move(move * Time.deltaTime * playerSpeed);
            if (move != Vector3.zero)
            {
                gameObject.transform.DORotateQuaternion(Quaternion.AngleAxis(Vector3.SignedAngle(Vector3.forward, move, Vector3.up), Vector3.up), 0.125f);
                anim.SetBool("isWalking", true);
            }
            else
            {
                anim.SetBool("isWalking", false);
            }
        }
        else
        {
            controller.Move(Vector3.zero * Time.deltaTime * playerSpeed);
            anim.SetBool("isWalking", false);
        }
    }
}
